package com.company;

import java.io.*;

public class Main {

    public static void main(String[] args) {

        String directori = "resources";

        String fichero1 = "fichero1.txt";

        String fichero2 = "fichero2.txt";

        String string = "Hello World";

        File file1 = new File(directori+File.separator+fichero1);

        File file2 = new File(directori+File.separator+fichero2);

        insertarEnFichero(file1,string);
        verContenidoFichero(directori+File.separator+fichero1);

    }

    public static void crearFichero(String directorio,String nombreFichero){

        File archivo = new File(directorio,nombreFichero);

    }

    public static void verContenidoFichero(String directorio){

        File archivo = new File(directorio);

        if (!archivo.exists()){

            System.out.println("No existe el directorio");
            return;
        }

        try {
            FileReader fileReaderArchivo = new FileReader(archivo);

            int character;
            do {

                character = fileReaderArchivo.read();
                System.out.print((char) character);

            }while (character!=-1);

            fileReaderArchivo.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (java.io.IOException i){
            i.printStackTrace();
        }
    }
    public static void insertarEnFichero(File file,String textoParaInsertar){

        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(textoParaInsertar);
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
